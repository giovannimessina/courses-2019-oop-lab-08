package it.unibo.oop.lab.simplegui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.InputMethodEvent;
import java.awt.event.InputMethodListener;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Scanner;

import javax.swing.*;

/**
 * 
 * This is my implementation of MiniGUI, this will not be a nice class
 * but i hope run :) .
 * 
 */
public final class MiniGuiImpl extends JPanel{
   
    int x = 0, y = 0;
    int angleX = 1, angleY = 1;
    Scanner scanner = new Scanner(System.in);
    
    private void move() {
        if(x + angleX <0) {
            angleX = 1;
            
        }
        else if(x+angleX > getWidth() -50) {
            angleX = -1;
        }else if(y + angleY <0) {
            angleY = 1;
        }
        else if(y+angleY > getHeight() -50) {
            angleY = -1;
        }
       
        x = x + angleX;
        y = y + angleY;
        System.out.println(scanner.toString());
        
    }
    
    @Override
    public void paint ( final Graphics g ) {
        super.paint(g);
        g.fillOval(x, y, 50, 50);
        
    }
      
    /**
     * 
     * Method that display the window.
     * 
     */
   
    /**
     * 
     * Main of the Application.
     * 
     * @param args 
     *              Default argoments.
     * @throws InterruptedException 
     */
public static void main(String[] args) throws InterruptedException{
    final JFrame frame = new JFrame("SECONDA FINESTRA");
    frame.setSize(new Dimension(600,600));
    frame.setMinimumSize(new Dimension(200,200));
    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    MiniGuiImpl app = new MiniGuiImpl();
    frame.add(app);
    frame.setVisible(true);
    frame.setResizable(false);
    
    
    while(true){
        app.move();
        app.repaint();
        Thread.sleep(10);
    }
}

}
