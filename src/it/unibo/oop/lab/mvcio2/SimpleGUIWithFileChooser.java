package it.unibo.oop.lab.mvcio2;

import it.unibo.oop.lab.mvcio.Controller;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUIWithFileChooser {
    private final JFrame frame =new JFrame("SimpleGUIWithFileChooser");
    private Dimension dimensionScreen = Toolkit.getDefaultToolkit().getScreenSize();
    
    public SimpleGUIWithFileChooser() {
       JPanel panel = new JPanel(new BorderLayout());
       frame.setContentPane(panel);
       
       JButton browse = new JButton("Browse");
       JTextArea text = new JTextArea();
       panel.add(browse, BorderLayout.NORTH);
       panel.add(text, BorderLayout.CENTER);
       
       browse.addActionListener(new ActionListener() {

       public void actionPerformed(ActionEvent e) {
               int cm;
               JFileChooser chooser = new JFileChooser();
               cm = chooser.showOpenDialog(panel);
              switch (cm) {
              
              case JFileChooser.APPROVE_OPTION: 
                 
                   Controller controller = new Controller();
                   controller.setFile(chooser.getSelectedFile().toPath());
                   try {
                    final List<String> lines = Files.readAllLines(chooser.getSelectedFile().toPath());
                    text.setText(lines.toString());
                   
                } catch (IOException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                   break;
               
              case JFileChooser.CANCEL_OPTION: 
                   
                  break;
               }
        
           
       }
           });
       
    }
   
   public void display() {
       
       frame.setSize(new Dimension(dimensionScreen.height/4,dimensionScreen.width/4));
       
       frame.setVisible(true);
       
   }
    
    /*
     * TODO: Starting from the application in mvcio:
     * 
     * 1) Add a JTextField and a button "Browse..." on the upper part of the
     * graphical interface.
     * Suggestion: use a second JPanel with a second BorderLayout, put the panel
     * in the North of the main panel, put the text field in the center of the
     * new panel and put the button in the line_end of the new panel.
     * 
     * 2) The JTextField should be non modifiable. And, should display the
     * current selected file.
     * 
     * 3) On press, the button should open a JFileChooser. The program should
     * use the method showSaveDialog() to display the file chooser, and if the
     * result is equal to JFileChooser.APPROVE_OPTION the program should set as
     * new file in the Controller the file chosen. If CANCEL_OPTION is returned,
     * then the program should do nothing. Otherwise, a message dialog should be
     * shown telling the user that an error has occurred (use
     * JOptionPane.showMessageDialog()).
     * 
     * 4) When in the controller a new File is set, also the graphical interface
     * must reflect such change. Suggestion: do not force the controller to
     * update the UI: in this example the UI knows when should be updated, so
     * try to keep things separated.
     */

   
   public static void main(String[] args) {
       new SimpleGUIWithFileChooser().display();
   }
}
