package it.unibo.oop.lab.iogui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.event.AncestorListener;

/**
 * This class is a simple application that writes a random number on a file.
 * 
 * This application does not exploit the model-view-controller pattern, and as
 * such is just to be used to learn the basics, not as a template for your
 * applications.
 */
public class BadIOGUI {

    private static final String TITLE = "A very simple GUI application";
    private static final String PATH = System.getProperty("user.home")
            + System.getProperty("file.separator")
            + BadIOGUI.class.getSimpleName() + ".txt";
    private static final int PROPORTION = 5;
    private final Random rng = new Random();
    private final JFrame frame = new JFrame(TITLE);

    /**
     * 
     */
    public BadIOGUI() {
        final JPanel canvas = new JPanel();
        canvas.setLayout(new BorderLayout());
        final JButton write = new JButton("Write on file");
        canvas.add(write, BorderLayout.CENTER);
        frame.setContentPane(canvas);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        /*
         * Handlers
         */
        write.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                /*
                 * This would be VERY BAD in a real application.
                 * 
                 * This makes the Event Dispatch Thread (EDT) work on an I/O
                 * operation. I/O operations may take a long time, during which
                 * your UI becomes completely unresponsive.
                 */
                try (PrintStream ps = new PrintStream(PATH)) {
//                    JPanel tmp = new JPanel(new BorderLayout());
//                    tmp.removeAll();
//                    frame.setContentPane(tmp);
//                    JTextArea text = new JTextArea("Write Here...");
//                    JButton send = new JButton("Send.");
//                    tmp.add(text,BorderLayout.CENTER);
//                    tmp.add(send,BorderLayout.SOUTH);
//                    tmp.setVisible(true);
//                    send.addActionListener(new ActionListener() {
//
//                       
//                        public void actionPerformed(ActionEvent e) {
//                            ps.print(text.getText());
//                        }});
//                    
                    JButton add = new JButton("AFSD");
                    canvas.add(add);
                    canvas.validate();
                    frame.validate();
                } catch (FileNotFoundException e1) {
                    JOptionPane.showMessageDialog(frame, e1, "Error", JOptionPane.ERROR_MESSAGE);
                    e1.printStackTrace();
                }
                finally {
                    
                    
                }
            }
        });
        /**
         * My code : - (
         * 
         */
        
        JPanel pan = new JPanel(new FlowLayout());
        frame.setContentPane(pan);
        JButton write2 = new JButton("Write");
        pan.add(write2);
        write2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                /*
                 * This would be VERY BAD in a real application.
                 * 
                 * This makes the Event Dispatch Thread (EDT) work on an I/O
                 * operation. I/O operations may take a long time, during which
                 * your UI becomes completely unresponsive.
                 */
                try (PrintStream ps = new PrintStream(PATH)) {
                    JPanel tmp = new JPanel(new BorderLayout());
                    tmp.removeAll();
                    frame.setContentPane(tmp);
                    JTextArea text = new JTextArea("Write Here...");
                    JButton send = new JButton("Send.");
                    tmp.add(text, BorderLayout.CENTER );
                    tmp.add(send, BorderLayout.SOUTH );
                    text.addFocusListener(new FocusListener() {

                        public void focusGained(FocusEvent e) {
                            text.setText(""); 
                        }

                        @Override
                        public void focusLost(FocusEvent e) {
                            // TODO Auto-generated method stub
                            
                        }});
                    
                    
                    send.addActionListener(new ActionListener() {

                       
                        public void actionPerformed(final ActionEvent e) {
                            ps.print(text.getLineCount());
                            frame.setContentPane(pan);
                            frame.validate();
                        }
                            });
      
                    frame.validate();
                } catch (FileNotFoundException e1) {
                    JOptionPane.showMessageDialog(frame, e1, "Error", JOptionPane.ERROR_MESSAGE);
                    e1.printStackTrace();
                }
                finally {
                    
                    
                }
            }
        });        
        JButton read = new JButton("Read");
        pan.add(read);
        read.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try(FileReader file = new FileReader(PATH)){
                List<String> l = Files.readAllLines(Path.of(PATH));
                for(String line : l) {
                    System.out.println(line);
                }
                
                
                }catch(IOException e3) {
                    System.out.println("Error" + e3);
                
                }
            
         }
        });
        
        
        /**
         * 
         * End of my code. 
         */
    }

    private void display() {
        /*
         * Make the frame one fifth the resolution of the screen. This very method is
         * enough for a single screen setup. In case of multiple monitors, the
         * primary is selected. In order to deal coherently with multimonitor
         * setups, other facilities exist (see the Java documentation about this
         * issue). It is MUCH better than manually specify the size of a window
         * in pixel: it takes into account the current resolution.
         */
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / PROPORTION, sh / PROPORTION);
        /*
         * Instead of appearing at (0,0), upper left corner of the screen, this
         * flag makes the OS window manager take care of the default positioning
         * on screen. Results may vary, but it is generally the best choice.
         */
        frame.setLocationByPlatform(true);
        /*
         * OK, ready to pull the frame onscreen
         */
        frame.setVisible(true);
//        frame.pack();
        
        
    }

    /**
     * @param args ignored
     */
    public static void main(final String... args) {
       new BadIOGUI().display();
    }
}
