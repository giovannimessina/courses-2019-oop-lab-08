package it.unibo.oop.lab.mvcio;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.IOException;

import javax.swing.*;
import javax.swing.event.AncestorListener;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUI {

    private final JFrame frame = new JFrame("My first Graphical Java Interface :-)");

    /*
     * Once the Controller is done, implement this class in such a way that:
     * 
     * 1) I has a main method that starts the graphical application
     * 
     * 2) In its constructor, sets up the whole view
     * 
     * 3) The graphical interface consists of a JTextArea with a button "Save" right
     * below (see "ex02.png" for the expected result). SUGGESTION: Use a JPanel with
     * BorderLayout
     * 
     * 4) By default, if the graphical interface is closed the program must exit
     * (call setDefaultCloseOperation)
     * 
     * 5) The program asks the controller to save the file if the button "Save" gets
     * pressed.
     * 
     * Use "ex02.png" (in the res directory) to verify the expected aspect.
     */
   

    /**
     * builds a new {@link SimpleGUI}.
     */
    public SimpleGUI() {
        
        JPanel panel = new JPanel(new BorderLayout());
        frame.setContentPane(panel);
        JTextArea text = new JTextArea();
        text.addFocusListener(new FocusListener() {

            @Override
            public void focusGained(final FocusEvent e) {
               text.setText("");
            }

            @Override
            public void focusLost(final FocusEvent e) {
                // TODO Auto-generated method stub
                
            }
                
            });
        panel.add(text);
        JButton save = new JButton("Save!");
        panel.add(save, BorderLayout.SOUTH);
        save.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                Controller controller = new Controller();
                try {
                    controller.writeFromFile(text.getText());
                    JOptionPane.showMessageDialog(panel, "Saved in file" + controller.getFile());
                    
                } catch (IOException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } 
                
            }});
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        
        /*
         * Make the frame half the resolution of the screen. This very method is
         * enough for a single screen setup. In case of multiple monitors, the
         * primary is selected.
         * 
         * In order to deal coherently with multimonitor setups, other
         * facilities exist (see the Java documentation about this issue). It is
         * MUCH better than manually specify the size of a window in pixel: it
         * takes into account the current resolution.
         */
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 2, sh / 2);
        /*
         * Instead of appearing at (0,0), upper left corner of the screen, this
         * flag makes the OS window manager take care of the default positioning
         * on screen. Results may vary, but it is generally the best choice.
         */
        frame.setLocationByPlatform(true);
        frame.setVisible(true);
    }

    /**
     * 
     * Main of the App.
     * @param args 
     *          casual args.
     */
 public static void main(final String[] args) {
        new SimpleGUI();
    }
}
