package it.unibo.oop.lab.mvcio;

import java.io.*;
import java.nio.file.Path;

/**
 * 
 * A simple controller responsible of I/O access.
 *  It considers a single file at a time, and it is able to serialize objects in it.
     
 */
public class Controller {

    private File file;
    private String path = System.getProperty("user.home")  + System.getProperty("file.separator") + "output.txt";
    /**
     * Constructor, idk what to do.
     */
    public Controller() {
        file = new File(path);
    }
    /**
     *  A method for setting a File as current file.
     * @param path
     *          Path of the current file.
     */
    public void setFile(final Path path) {
        file = path.toFile();
    }
/**
 * A method for getting the current File
 * @return Path of the file.
 */
    public String getFile() {
        return file.getPath();
    }
    
    /**
     * A method that gets a String as input and saves its content on the current
     * file. This method may throw an IOException.
     * @param content
     *              String that must be write inside the file.
     * @throws IOException
     */
    public void writeFromFile(final String content) throws IOException {
        
        try (PrintStream ps = new PrintStream(file)){
            ps.print(content);
        } catch (IOException e) {
            System.out.println("\nERROR: " + e);
        }
    }
    /*
     * 
     * Implement this class with:
     * 
     * 1)
     * 
     * 2) 
     * 
     * 3) A method for getting the path (in form of String) of the current File
     * 
     * 4) 
     * 
     * 5) By default, the current file is "output.txt" inside the user home folder.
     * A String representing the local user home folder can be accessed using
     * System.getProperty("user.home"). The separator symbol (/ on *nix, \ on
     * Windows) can be obtained as String through the method
     * System.getProperty("file.separator"). The combined use of those methods leads
     * to a software that runs correctly on every platform.
     */

}
